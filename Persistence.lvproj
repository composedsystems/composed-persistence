﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="externals" Type="Folder">
			<Item Name="composed-data-types" Type="Folder">
				<Item Name="Assertion 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Assertion 1D/Assertion 1D.lvclass"/>
				<Item Name="Assertion.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Assertion/Assertion.lvclass"/>
				<Item Name="Boolean 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Boolean 1D/Boolean 1D.lvclass"/>
				<Item Name="Boolean.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Boolean/Boolean.lvclass"/>
				<Item Name="Data Type.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Data Type/Data Type.lvclass"/>
				<Item Name="Double 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Double 1D/Double 1D.lvclass"/>
				<Item Name="Double.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Double/Double.lvclass"/>
				<Item Name="I32 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/I32 1D/I32 1D.lvclass"/>
				<Item Name="I32.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/I32/I32.lvclass"/>
				<Item Name="Path 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Path 1D/Path 1D.lvclass"/>
				<Item Name="Path.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Path/Path.lvclass"/>
				<Item Name="String 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/String 1D/String 1D.lvclass"/>
				<Item Name="String.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/String/String.lvclass"/>
				<Item Name="Timestamp.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/Timestamp/Timestamp.lvclass"/>
				<Item Name="U32 1D.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/U32 1D/U32 1D.lvclass"/>
				<Item Name="U32.lvclass" Type="LVClass" URL="../externals/composed-data-types/source/U32/U32.lvclass"/>
			</Item>
			<Item Name="composed-run-time-assertions" Type="Folder">
				<Item Name="Run Time Assertions.lvlib" Type="Library" URL="../externals/composed-run-time-assertions/Source/Run Time Assertions.lvlib"/>
			</Item>
		</Item>
		<Item Name="CAML.lvlib" Type="Library" URL="../source/CAML/CAML.lvlib"/>
		<Item Name="IPersist.lvlib" Type="Library" URL="../source/IPersist/IPersist.lvlib"/>
		<Item Name="XML.lvlib" Type="Library" URL="../source/XML/XML.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
